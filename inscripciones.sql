CREATE DATABASE inscripciones;
\c inscripciones

CREATE SEQUENCE horario_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE horario(
	id INTEGER NOT NULL DEFAULT nextval('horario_seq'),
	dia VARCHAR(9) NOT NULL,
	hora VARCHAR(13) NOT NULL,
	CONSTRAINT pk_horario PRIMARY KEY (id)
);

CREATE SEQUENCE tipo_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE tipo(
	id INTEGER NOT NULL DEFAULT nextval('tipo_seq'),
	tipo VARCHAR(30) NOT NULL,
	CONSTRAINT pk_tipo PRIMARY KEY (id)
);

CREATE SEQUENCE carrera_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE carrera(
	id INTEGER NOT NULL DEFAULT nextval('carrera_seq'),
	clave VARCHAR(10) NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	CONSTRAINT pk_carrera PRIMARY KEY (id)
);

CREATE SEQUENCE semestre_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE semestre(
	id INTEGER NOT NULL DEFAULT nextval('semestre_seq'),
	semestre VARCHAR(7) NOT NULL,
	CONSTRAINT pk_semestre PRIMARY KEY (id)
);

CREATE SEQUENCE profesor_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE profesor(
	id INTEGER NOT NULL DEFAULT nextval('profesor_seq'),
	nombre VARCHAR(30) NOT NULL,
	aPaterno VARCHAR(30) NOT NULL,
	aMaterno VARCHAR(30) NULL,
	CONSTRAINT pk_profesor PRIMARY KEY (id)
);

CREATE SEQUENCE asignatura_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE asignatura(
	id INTEGER NOT NULL DEFAULT nextval('asignatura_seq'),
	clave VARCHAR(8) NOT NULL,
	nombre VARCHAR(40) NOT NULL,
	CONSTRAINT pk_asignatura PRIMARY KEY (id)
);

CREATE SEQUENCE grupo_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE grupo(
	id INTEGER NOT NULL DEFAULT nextval('grupo_seq'),
	clave VARCHAR(6) NOT NULL,
	salon VARCHAR(6) NOT NULL, --este campo puede ser agregado en la tabla transitiva grupoxhorario
	cupo INTEGER NOT NULL,
	semestre INTEGER NOT NULL,
	CONSTRAINT pk_grupo PRIMARY KEY (id),
	CONSTRAINT fk_semestre FOREIGN KEY (semestre) REFERENCES semestre (id)
);

CREATE SEQUENCE users_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE users(
	id INTEGER NOT NULL DEFAULT nextval('users_seq'),
	username VARCHAR(20) NOT NULL UNIQUE,
	password VARCHAR(60) NOT NULL,
	tipo INTEGER NOT NULL,
	CONSTRAINT pk_users PRIMARY KEY (id),
	CONSTRAINT fk_tipo FOREIGN KEY (tipo) REFERENCES tipo (id)
);

CREATE SEQUENCE estudiante_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE estudiante(
	id INTEGER NOT NULL DEFAULT nextval('estudiante_seq'),
	usuario INTEGER NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	aPaterno VARCHAR(30) NOT NULL,
	aMaterno VARCHAR(30) NOT NULL,
	correo VARCHAR(50) NOT NULL UNIQUE,
	--curp VARCHAR(18) NOT NULL,
	carrera INTEGER NOT NULL,
	semestre INTEGER NOT NULL,
	CONSTRAINT pk_estudiante PRIMARY KEY (id),
	CONSTRAINT fk_usuario FOREIGN KEY (usuario) REFERENCES users(id),
	CONSTRAINT fk_carrera FOREIGN KEY (carrera) REFERENCES carrera (id),
	CONSTRAINT fk_estudiante_semestre FOREIGN KEY (semestre) REFERENCES semestre (id)
);

CREATE SEQUENCE direccion_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE direccion(
	id INTEGER NOT NULL DEFAULT nextval('direccion_seq'),
	estudiante INTEGER NOT NULL,
	calle VARCHAR(50) NOT NULL,
	numeroInt VARCHAR(10) NOT NULL,
	numeroExt VARCHAR(10) NULL,
	colonia VARCHAR(50) NOT NULL,
	delegacion VARCHAR(50) NOT NULL,
	cp VARCHAR(5) NOT NULL,
	estado VARCHAR(22) NOT NULL,
	pais VARCHAR(22) NOT NULL,
	CONSTRAINT pk_direccion PRIMARY KEY (id),
	CONSTRAINT fk_estudiante FOREIGN KEY (estudiante) REFERENCES estudiante (id)
);

CREATE SEQUENCE estudiantexgrupo_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START WITH 1
	NO CYCLE;

CREATE TABLE estudianteXgrupo(
	id INTEGER NOT NULL DEFAULT nextval('estudiantexgrupo_seq'),
	estudiante INTEGER NOT NULL,
	grupo INTEGER NOT NULL,
	CONSTRAINT pk_estudiante_grupo PRIMARY KEY (id),
	CONSTRAINT fk_estudiante_estudiantexgrupo FOREIGN KEY (estudiante) REFERENCES estudiante (id),
	CONSTRAINT fk_grupo_estudiantexgrupo FOREIGN KEY (grupo) REFERENCES grupo (id)
);

CREATE SEQUENCE estudiantexasignatura_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START WITH 1
	NO CYCLE;

CREATE TABLE estudianteXasignatura(
	id INTEGER NOT NULL DEFAULT nextval('estudiantexasignatura_seq'),
	estudiante INTEGER NOT NULL,
	asignatura INTEGER NOT NULL,
	semestre INTEGER NOT NULL,
	estatus BOOLEAN NOT NULL DEFAULT FALSE,
	CONSTRAINT pk_estudiante_asignatura PRIMARY KEY (id),
	CONSTRAINT fk_estudiante_estudianteXasignatura FOREIGN KEY (estudiante) REFERENCES estudiante (id),
	CONSTRAINT fk_asignatura_estudianteXasignatura FOREIGN KEY (asignatura) REFERENCES asignatura (id),
	CONSTRAINT fk_semestre_estudianteXasignatura FOREIGN KEY (semestre) REFERENCES semestre (id)
);

CREATE SEQUENCE asignaturaxgrupo_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START WITH 1
	NO CYCLE;

CREATE TABLE asignaturaXgrupo(
	id INTEGER NOT NULL DEFAULT nextval('asignaturaxgrupo_seq'),
	asignatura INTEGER NOT NULL,
	grupo INTEGER NOT NULL,
	CONSTRAINT pk_asignatura_grupo PRIMARY KEY (id),
	CONSTRAINT fk_asignatura_asignaturaxgrupo FOREIGN KEY (asignatura) REFERENCES asignatura (id),
	CONSTRAINT fk_grupo_asignaturaxgrupo FOREIGN KEY (grupo) REFERENCES grupo (id)
);

CREATE SEQUENCE grupoxprofesor_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START WITH 1
	NO CYCLE;

CREATE TABLE grupoXprofesor(
	id INTEGER NOT NULL DEFAULT nextval('grupoxprofesor_seq'),
	grupo INTEGER NOT NULL,
	profesor INTEGER NOT NULL,
	CONSTRAINT pk_grupo_profesor PRIMARY KEY (id),
	CONSTRAINT fk_grupo_grupoxprofesor FOREIGN KEY (grupo) REFERENCES grupo (id),
	CONSTRAINT fk_profesor_grupoxprofesor FOREIGN KEY (profesor) REFERENCES profesor (id)
);

CREATE SEQUENCE grupoxhorario_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE
	START WITH 1
	NO CYCLE;

CREATE TABLE grupoXhorario(
	id INTEGER NOT NULL DEFAULT nextval('grupoxhorario_seq'),
	grupo INTEGER NOT NULL,
	horario INTEGER NOT NULL,
	--salon VARCHAR(6) NOT NULL,
	CONSTRAINT pk_grupo_horario PRIMARY KEY (id),
	CONSTRAINT fk_grupo_grupoxhorario FOREIGN KEY (grupo) REFERENCES grupo (id),
	CONSTRAINT fk_horario_grupoxhorario FOREIGN KEY (horario) REFERENCES horario (id)
);

INSERT INTO horario(dia, hora) VALUES('L y Mc', '07:00 - 09:00');
INSERT INTO horario(dia, hora) VALUES('L y Mc', '09:00 - 11:00');
INSERT INTO horario(dia, hora) VALUES('L y Mc', '11:00 - 13:00');
INSERT INTO horario(dia, hora) VALUES('M y J', '07:00 - 09:00');
INSERT INTO horario(dia, hora) VALUES('M y J', '09:00 - 11:00');
INSERT INTO horario(dia, hora) VALUES('M y J', '11:00 - 13:00');
INSERT INTO horario(dia, hora) VALUES('V y Sa', '07:00 - 09:00');
INSERT INTO horario(dia, hora) VALUES('V y Sa', '09:00 - 11:00');
INSERT INTO horario(dia, hora) VALUES('V y Sa', '11:00 - 13:00');

INSERT INTO tipo(tipo) VALUES('Alumno');
INSERT INTO tipo(tipo) VALUES('Administrador');

INSERT INTO carrera(clave, nombre) VALUES('305','Derecho');

INSERT INTO semestre(semestre) VALUES('Primer');
INSERT INTO semestre(semestre) VALUES('Segundo');
INSERT INTO semestre(semestre) VALUES('Tercero');
INSERT INTO semestre(semestre) VALUES('Cuarto');
INSERT INTO semestre(semestre) VALUES('Quinto');
INSERT INTO semestre(semestre) VALUES('Sexto');
INSERT INTO semestre(semestre) VALUES('Séptimo');
INSERT INTO semestre(semestre) VALUES('Octavo');

INSERT INTO profesor(nombre, aPaterno, aMaterno) VALUES('Profesor', 'de', 'Derecho');
INSERT INTO profesor(nombre, aPaterno, aMaterno) VALUES('El', 'otro', 'profesor');
INSERT INTO profesor(nombre, aPaterno, aMaterno) VALUES('El', 'profe', 'barco');
INSERT INTO profesor(nombre, aPaterno) VALUES('Nuevo', 'Profesor');
INSERT INTO profesor(nombre, aPaterno) VALUES('Siguiente', 'maestro');
INSERT INTO profesor(nombre, aPaterno) VALUES('Mr.', 'Prof');

INSERT INTO asignatura(clave, nombre) VALUES('1110', 'Derecho Romano I');
INSERT INTO asignatura(clave, nombre) VALUES('1111', 'Historia del Derecho Mexicano');
INSERT INTO asignatura(clave, nombre) VALUES('1113', 'Introducción al Estudio del Derecho');
INSERT INTO asignatura(clave, nombre) VALUES('1114', 'Teoría General del Estado');
INSERT INTO asignatura(clave, nombre) VALUES('1116', 'Sociología Jurídica y General');
INSERT INTO asignatura(clave, nombre) VALUES('1132', 'Cómputo');
INSERT INTO asignatura(clave, nombre) VALUES('1210', 'Acto Jurídico y Personas');
INSERT INTO asignatura(clave, nombre) VALUES('1212', 'Derecho Romano II');
INSERT INTO asignatura(clave, nombre) VALUES('1214', 'Metodología Jurídica');
INSERT INTO asignatura(clave, nombre) VALUES('1216', 'Teoría del Derecho');
INSERT INTO asignatura(clave, nombre) VALUES('1218', 'Teoría de la Ley Penal y del Delito');
INSERT INTO asignatura(clave, nombre) VALUES('1220', 'Teoría de la Constitución');
INSERT INTO asignatura(clave, nombre) VALUES('1308', 'Bienes y Derechos Reales');
INSERT INTO asignatura(clave, nombre) VALUES('1309', 'Delitos en Particular');
INSERT INTO asignatura(clave, nombre) VALUES('1310', 'Derecho Constitucional');
INSERT INTO asignatura(clave, nombre) VALUES('1311', 'Sistemas Jurídicos');
INSERT INTO asignatura(clave, nombre) VALUES('1313', 'Teoría del Proceso');
INSERT INTO asignatura(clave, nombre) VALUES('1317', 'Teoría Económica');
INSERT INTO asignatura(clave, nombre) VALUES('1415', 'Derecho Procesal Civil');
INSERT INTO asignatura(clave, nombre) VALUES('1416', 'Derecho Administrativo I');
INSERT INTO asignatura(clave, nombre) VALUES('1417', 'Derecho Económico');
INSERT INTO asignatura(clave, nombre) VALUES('1419', 'Garantías Constitucionales');
INSERT INTO asignatura(clave, nombre) VALUES('1422', 'Obligaciones');
INSERT INTO asignatura(clave, nombre) VALUES('1429', 'Sociedades Mercantiles');
INSERT INTO asignatura(clave, nombre) VALUES('1512', 'Contratos Civiles');
INSERT INTO asignatura(clave, nombre) VALUES('1514', 'Derecho Procesal Penal');
INSERT INTO asignatura(clave, nombre) VALUES('1516', 'Derecho Internacional Público');
INSERT INTO asignatura(clave, nombre) VALUES('1517', 'Derecho Administrativo II');
INSERT INTO asignatura(clave, nombre) VALUES('1518', 'Régimen Jurídico de Comercio Exterior');
INSERT INTO asignatura(clave, nombre) VALUES('1519', 'Título y Operaciones de Crédito');
INSERT INTO asignatura(clave, nombre) VALUES('1611', 'Contratos Mercantiles');
INSERT INTO asignatura(clave, nombre) VALUES('1613', 'Derecho Fiscal I');
INSERT INTO asignatura(clave, nombre) VALUES('1614', 'Derecho Internacional Privado I');
INSERT INTO asignatura(clave, nombre) VALUES('1615', 'Derecho Individual del Trabajo');
INSERT INTO asignatura(clave, nombre) VALUES('1616', 'Filosofía del Derecho');
INSERT INTO asignatura(clave, nombre) VALUES('1617', 'Derecho Familiar');
INSERT INTO asignatura(clave, nombre) VALUES('1722', 'Derecho Bancario y Bursátil');
INSERT INTO asignatura(clave, nombre) VALUES('1723', 'Amparo');
INSERT INTO asignatura(clave, nombre) VALUES('1724', 'Derecho Fiscal II');
INSERT INTO asignatura(clave, nombre) VALUES('1727', 'Derecho Internacional Privado II');
INSERT INTO asignatura(clave, nombre) VALUES('1728', 'Derecho Colectivo y Procesal del Trabajo');
INSERT INTO asignatura(clave, nombre) VALUES('1729', 'Derecho Sucesorio');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Derecho Agrario');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Seguridad Social');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Argumentación Jurídica');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Lógica Jurídica');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Juicios Orales en Materia Civil');
INSERT INTO asignatura(clave, nombre) VALUES('1826', 'Juicios Orales en Materia Penal');

INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1145', 'A-001', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1146', 'A-002', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1147', 'A-003', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1148', 'A-004', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1149', 'A-005', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1154', 'A-006', 60, 1);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2241', 'B-101', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2242', 'B-102', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2244', 'B-103', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2245', 'B-104', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2246', 'B-105', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2249', 'B-106', 60, 2);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1335', 'C-201', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1336', 'C-202', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1337', 'C-203', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1338', 'C-204', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1342', 'C-205', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1345', 'C-206', 60, 3);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2436', 'D-201', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2437', 'D-202', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2438', 'D-203', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2442', 'D-204', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2445', 'D-205', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2449', 'D-206', 60, 4);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1543', 'A-201', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1544', 'A-202', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1545', 'A-203', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1546', 'A-204', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1547', 'A-205', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1549', 'A-206', 60, 5);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2666', 'D-101', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2667', 'D-102', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2668', 'D-103', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2669', 'D-104', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2672', 'D-105', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2674', 'D-106', 60, 6);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1754', 'B-201', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1755', 'B-202', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1756', 'B-203', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1757', 'B-204', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1759', 'B-205', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('1762', 'B-206', 60, 7);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2826', 'C-001', 60, 8);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2827', 'C-002', 60, 8);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2828', 'C-003', 60, 8);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2832', 'C-004', 60, 8);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2834', 'C-005', 60, 8);
INSERT INTO grupo(clave, salon, cupo, semestre) VALUES('2839', 'C-006', 60, 8);

INSERT INTO users(username, password, tipo) VALUES('juanbravo', '$2y$10$Hz/BAt2o5VYG47UXcnGrHeZLIb8gTN68mvALMhkOP.jm2fRX1er3a', 1);--password
INSERT INTO users(username, password, tipo) VALUES('admin', '$2y$10$8Aa3TG7/4Y9k8MuzTI/mHO.NIHHrzcLVaCRA0vJS9YgQo8C9SGbii', 2);--sekret

INSERT INTO estudiante(usuario, nombre, aPaterno, aMaterno, correo, carrera, semestre) VALUES(1, 'Juan', 'Bravo', 'Castro', 'jbravo@correo.com', 1, 7);

INSERT INTO direccion(estudiante, calle, numeroInt, numeroExt, colonia, delegacion, cp, estado, pais) VALUES(1, 'Avenida principal', '860', '911', 'Colonia Central', 'Delegación Principal', '03200', 'Ciudad de México', 'México');--not baked

INSERT INTO estudianteXgrupo(estudiante, grupo) VALUES(1, 7);

--INSERT INTO estudianteXasignatura(estudiante, asignatura, semestre) VALUES(1, 42, 7);

INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(1, 1);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(2, 2);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(3, 3);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(4, 4);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(5, 5);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(6, 6);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(7, 7);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(8, 8);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(9, 9);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(10, 10);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(11, 11);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(12, 12);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(13, 13);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(14, 14);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(15, 15);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(16, 16);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(17, 17);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(18, 18);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(19, 19);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(20, 20);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(21, 21);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(22, 22);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(23, 23);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(24, 24);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(25, 25);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(26, 26);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(27, 27);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(28, 28);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(29, 29);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(30, 30);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(31, 31);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(32, 32);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(33, 33);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(34, 34);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(35, 35);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(36, 36);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(37, 37);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(38, 38);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(39, 39);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(40, 40);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(41, 41);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(42, 42);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(43, 43);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(44, 44);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(45, 45);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(46, 46);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(47, 47);
INSERT INTO asignaturaXgrupo(asignatura, grupo) VALUES(48, 48);

INSERT INTO grupoXprofesor(grupo, profesor) VALUES(1, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(2, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(3, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(4, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(5, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(6, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(7, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(8, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(9, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(10, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(11, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(12, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(13, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(14, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(15, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(16, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(17, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(18, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(19, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(20, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(21, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(22, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(23, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(24, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(25, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(26, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(27, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(28, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(29, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(30, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(31, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(32, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(33, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(34, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(35, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(36, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(37, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(38, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(39, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(40, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(41, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(42, 6);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(43, 1);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(44, 2);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(45, 3);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(46, 4);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(47, 5);
INSERT INTO grupoXprofesor(grupo, profesor) VALUES(48, 6);

INSERT INTO grupoXhorario(grupo, horario) VALUES(1, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(2, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(3, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(4, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(5, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(6, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(7, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(8, 8);
INSERT INTO grupoXhorario(grupo, horario) VALUES(9, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(10, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(11, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(12, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(13, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(14, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(15, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(16, 8);
INSERT INTO grupoXhorario(grupo, horario) VALUES(17, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(18, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(19, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(20, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(21, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(22, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(23, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(24, 8);
INSERT INTO grupoXhorario(grupo, horario) VALUES(25, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(26, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(27, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(28, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(29, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(30, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(31, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(32, 8);
INSERT INTO grupoXhorario(grupo, horario) VALUES(33, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(34, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(35, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(36, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(37, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(38, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(39, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(40, 8);
INSERT INTO grupoXhorario(grupo, horario) VALUES(41, 1);
INSERT INTO grupoXhorario(grupo, horario) VALUES(42, 2);
INSERT INTO grupoXhorario(grupo, horario) VALUES(43, 3);
INSERT INTO grupoXhorario(grupo, horario) VALUES(44, 4);
INSERT INTO grupoXhorario(grupo, horario) VALUES(45, 5);
INSERT INTO grupoXhorario(grupo, horario) VALUES(46, 6);
INSERT INTO grupoXhorario(grupo, horario) VALUES(47, 7);
INSERT INTO grupoXhorario(grupo, horario) VALUES(48, 8);