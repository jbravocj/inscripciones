<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SemestreTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SemestreTable Test Case
 */
class SemestreTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SemestreTable
     */
    public $Semestre;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Semestre'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Semestre') ? [] : ['className' => SemestreTable::class];
        $this->Semestre = TableRegistry::getTableLocator()->get('Semestre', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Semestre);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
