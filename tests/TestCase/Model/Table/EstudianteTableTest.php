<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EstudianteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EstudianteTable Test Case
 */
class EstudianteTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EstudianteTable
     */
    public $Estudiante;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Estudiante'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Estudiante') ? [] : ['className' => EstudianteTable::class];
        $this->Estudiante = TableRegistry::getTableLocator()->get('Estudiante', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Estudiante);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
