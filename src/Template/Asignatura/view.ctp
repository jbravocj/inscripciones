<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['controller' => 'users','action' => 'inicio'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Consultar horarios', ['controller' => 'Grupo', 'action' => 'index'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item active">
              <?= $this->Html->link('Inscribir materias', ['controller' => 'Asignatura', 'action' => 'semestre'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Comprobante de inscripción', ['controller' => 'Estudiante', 'action' => 'comprobante'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Mi información', ['controller' => 'Estudiante'], array('class' => 'btn btn-outline-info')) ?> &nbsp;
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'users', 'action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div>
<div class="container">
    <h3>Escoge el grupo que deseas inscribir</h3>
    <table class="table table-sm">
        <thead class="thead-dark">
          <tr>
            <th class="text-center">Grupo</th>
            <th class="text-center">Asignatura</th>
            <th class="text-center">Profesor</th>
            <th class="text-center">Día y Hora</th>
            <th class="text-center">Salón</th>
            <th class="text-center">Cupo</th>
            <th class="text-center"></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($asignatura as $asignatura): ?>
            <tr>
                <td class="text-center"><?= $asignatura['Grupo']['clave'] ?></td>
                <td class="text-center"><?= $asignatura->nombre ?></td>
                <td class="text-center"><?= $asignatura['Profesor']['nombre'] . ' ' . $asignatura['Profesor']['apaterno'] . ' ' . $asignatura['Profesor']['amaterno'] ?></td>
                <td class="text-center"><?= $asignatura['Horario']['dia'] . ' ' . $asignatura['Horario']['hora'] ?></td>
                <td class="text-center"><?= $asignatura['Grupo']['salon'] ?></td>
                <td class="text-center"><?= $asignatura['Grupo']['cupo'] ?></td>
                <td class="text-center">
                    <?= $this->Html->link('Inscribirme', ['action' => 'inscribir', '?' => array('usuario' => $user->read('Auth.User.id'), 'asignatura' => $asignatura->id, 'semestre' => $asignatura['Semestre']['id'], 'grupo' => $asignatura['Grupo']['id'])], ['class' => 'btn btn-sm btn-primary', 'confirm' => '¿Estás seguro que te deseas inscribir al grupo ' . $asignatura['Grupo']['clave'] . '?']) ?>
                </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primero')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} de un total de {{count}} resultados')]) ?></p>
    </div>
</div>