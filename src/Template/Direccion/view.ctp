<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Direccion $direccion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Direccion'), ['action' => 'edit', $direccion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Direccion'), ['action' => 'delete', $direccion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $direccion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Direccion'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Direccion'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="direccion view large-9 medium-8 columns content">
    <h3><?= h($direccion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Calle') ?></th>
            <td><?= h($direccion->calle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numeroint') ?></th>
            <td><?= h($direccion->numeroint) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numeroext') ?></th>
            <td><?= h($direccion->numeroext) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Colonia') ?></th>
            <td><?= h($direccion->colonia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delegacion') ?></th>
            <td><?= h($direccion->delegacion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cp') ?></th>
            <td><?= h($direccion->cp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= h($direccion->estado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pais') ?></th>
            <td><?= h($direccion->pais) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($direccion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estudiante') ?></th>
            <td><?= $this->Number->format($direccion->estudiante) ?></td>
        </tr>
    </table>
</div>
