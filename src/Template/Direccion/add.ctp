<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Datos personales</title>
    <div>
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="">
              <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_fac.png" width="30" height="30" alt="">
              <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_unam.png" width="30" height="30" alt="">
              Sistema de inscripciones de la Facultad de Derecho de la UNAM
            </a>
        </nav>
    </div>
    <div class="container">
        <?= $this->Form->create($direccion) ?>
        <fieldset>
            <legend>Ingresa tu dirección</legend>
            <?php
                echo $this->Form->control('calle');
                echo $this->Form->control('numeroint', ['label' => ['text' => 'Número exterior']]);
                echo $this->Form->control('numeroext', ['label' => ['text' => 'Número interior']]);
                echo $this->Form->control('colonia');
                echo $this->Form->control('delegacion', ['label' => ['text' => 'Delegación o municipio']]);
                echo $this->Form->control('cp', ['label' => ['text' => 'Código postal']]);
                echo $this->Form->control('estado');
                echo $this->Form->control('pais', ['label' => ['text' => 'País']]);
            ?>
        </fieldset>
        <?= $this->Form->button('Guardar', array('class' => 'right btn btn-lg btn-success', 'style' => 'margin-bottom: 25px;')) ?>
        <?= $this->Form->end() ?>
    </div>