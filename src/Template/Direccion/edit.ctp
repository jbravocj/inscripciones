<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['controller' => 'users','action' => 'inicio'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Consultar horarios', ['controller' => 'Grupo', 'action' => 'index'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Inscribir materias', ['controller' => 'Asignatura', 'action' => 'semestre'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Comprobante de inscripción', ['controller' => 'Estudiante', 'action' => 'comprobante'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Mi información', ['controller' => 'Estudiante'], array('class' => 'btn btn-info')) ?> &nbsp;
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'users', 'action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div>
<div class="container">
    <?= $this->Form->create($direccion) ?>
    <fieldset>
        <legend>Editar domicilio</legend>
        <?php
            echo $this->Form->control('calle');
            echo $this->Form->control('numeroint', array('label' => 'Número exterior'));
            echo $this->Form->control('numeroext', array('label' => 'Número interior'));
            echo $this->Form->control('colonia');
            echo $this->Form->control('delegacion', array('label' => 'Delegación o municipio'));
            echo $this->Form->control('cp', array('label' => 'Código postal'));
            echo $this->Form->control('estado');
            echo $this->Form->control('pais', array('label' => 'País'));
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar', array('class' => 'right btn btn-lg btn-success', 'style' => 'margin-bottom: 25px;')) ?>
    <?= $this->Form->end() ?>
</div>
