<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Inicio de sesión</title>
<div>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="">
          <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_fac.png" width="30" height="30" alt="">
          <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_unam.png" width="30" height="30" alt="">
          Sistema de inscripciones de la Facultad de Derecho de la UNAM
        </a>
    </nav>
</div>
<div class='container'>
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend>Regístrate</legend>
        <?php
            echo $this->Form->control('username', ['label' => ['text' => 'Nombre de usuario']]);
            echo $this->Form->control('password', ['label' => ['text' => 'Contraseña']]);
        ?>
        <label>Repite tu nueva contraseña</label>
        <input type="password" name="passwd" required="required" id="passwd">
    </fieldset>
    <?= $this->Form->button('Regristrarme', array('class' => 'right btn btn-outline-success', 'disabled' => 'true')) ?>
    <?= $this->Html->link('Regresar', ['action' => 'login'], array('class' => 'right btn btn-outline-secondary', 'style' => 'margin-right: 10px;')) ?>
    <?= $this->Form->end() ?>
</div>
<script>
    $(document).ready(function(){
        $('input').change(function(){
          if (($('#password').val() !== '') && ($('#password').val() === $('#passwd').val())) {
              $(".btn-outline-success").prop('disabled', false);
            } else {
              $(".btn-outline-success").prop('disabled',true);
            }
        });
    });
</script>