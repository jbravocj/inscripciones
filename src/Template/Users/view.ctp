<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['action' => 'index'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Gestionar alumnos', ['action' => 'view'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'Users','action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div>
<div class="container">
    <h3>Selecciona el semestre de los alumnos que quieres ver</h3>
    <table class="table table-sm">
        <thead class="thead-dark">
            <tr>
              <th class="text-center">Semestre</th>
              <th class="text-center">Ver alumnos</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach ($semestre as $semestre): ?>
            <tr>
                <td class="text-center"><?= $semestre->semestre ?></td>
                <td class="text-center">
                    <?= $this->Html->link('Lista de alumnos', ['controller' => 'Estudiante', 'action' => 'view', $semestre->id], array('class' => 'btn btn-sm btn-primary')) ?>
                </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
</div>