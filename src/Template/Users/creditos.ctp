<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Créditos</title>
    </head>
    <body>
        <div>
            <nav class="navbar navbar-light bg-light">
                <a class="navbar-brand" href="#">
                  <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_fac.png" width="30" height="30" alt="">
                  <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_unam.png" width="30" height="30" alt="">
                  Sistema de inscripciones de la Facultad de Derecho de la UNAM
                </a>
            </nav>
        </div>
        <div class="container">
            <div class="card text-center" style="margin-top: 25px;">
                <div class="card-header">Créditos</div>
                <div class="card-body">
                  <h5 class="card-title">Integrantes</h5>
                  <p class="card-text">Bravo Castro Juan Manuel</p>
                  <p class="card-text">Chamorro Parra Guillermo Gamaliel</p>
                  <p class="card-text">González Policarpo Josué</p>
                  <p class="card-text">Guillen Martínez Felipe Itzamatul</p>
                  <p class="card-text">Sánchez García Erick</p>
                  <p class="card-text">Silva García David</p>
                </div>
                <div class="card-footer text-muted">
                  <?= $this->Html->link('Regresar', ['action' => 'login'], array('class' => 'btn btn-primary')) ?>
                </div>
            </div>
        </div>
    </body>
</html>

