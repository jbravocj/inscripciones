<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Inicio de sesión</title>
        <script src='https://www.google.com/recaptcha/api.js?render=6Lca9YoUAAAAACfUvsMxOksKm-dJ-r1zB6IbDCjx'></script>
    </head>
    <body>
        <div>
            <nav class="navbar navbar-light bg-light">
                <a class="navbar-brand" href="">
                  <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_fac.png" width="30" height="30" alt="">
                  <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_unam.png" width="30" height="30" alt="">
                  Sistema de inscripciones de la Facultad de Derecho de la UNAM
                </a>
            </nav>
        </div>
        <div class="container">
            <h1>Inicio de sesión</h1>
            <?= $this->Form->create() ?>
            <?= $this->Form->control('username', ['label' => ['text' => 'Nombre de usuario'], 'required' => 'true']) ?>
            <?= $this->Form->control('password', ['label' => ['text' => 'Contraseña'], 'required' => 'true']) ?>
            <?= $this->Form->button('Iniciar sesión', array('class' => 'right btn btn-lg btn-primary')) ?>
            <?= $this->Form->end() ?>
            <?= $this->Html->link('¡Regístrate!', ['action' => 'registro']) ?>
            <?= $this->Html->link('Créditos', ['action' => 'creditos']) ?>
        </div>
    </body>
</html>