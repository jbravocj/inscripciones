<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Semestre $semestre
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $semestre->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $semestre->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Semestre'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="semestre form large-9 medium-8 columns content">
    <?= $this->Form->create($semestre) ?>
    <fieldset>
        <legend><?= __('Edit Semestre') ?></legend>
        <?php
            echo $this->Form->control('semestre');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
