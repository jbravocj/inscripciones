<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Semestre $semestre
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Semestre'), ['action' => 'edit', $semestre->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Semestre'), ['action' => 'delete', $semestre->id], ['confirm' => __('Are you sure you want to delete # {0}?', $semestre->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Semestre'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Semestre'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="semestre view large-9 medium-8 columns content">
    <h3><?= h($semestre->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Semestre') ?></th>
            <td><?= h($semestre->semestre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($semestre->id) ?></td>
        </tr>
    </table>
</div>
