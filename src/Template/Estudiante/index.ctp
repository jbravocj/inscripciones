<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['controller' => 'users','action' => 'inicio'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Consultar horarios', ['controller' => 'Grupo', 'action' => 'index'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Inscribir materias', ['controller' => 'Asignatura', 'action' => 'semestre'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Comprobante de inscripción', ['controller' => 'Estudiante', 'action' => 'comprobante'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Mi información', ['controller' => 'Estudiante'], array('class' => 'btn btn-info')) ?> &nbsp;
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'users', 'action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div>
<div class="row">
    <div class="col-6">
        <div class="card" style="margin-top: 25px; margin-left: 50px;">
            <div class="card-header">Datos personales</div>
            <div class="card-body">
              <p class="card-title">Nombre</p>
              <h5 class="card-text"><?= $estudiante->nombre . ' ' . $estudiante->apaterno . ' ' . $estudiante->amaterno ?></h5>
            </div>
            <div class="card-body">
              <p class="card-title">Correo electrónico</p>
              <h5 class="card-text"><?= $estudiante->correo ?></h5>
            </div>
            <div class="card-body">
              <p class="card-title">Dirección</p>
              <h5 class="card-text"><?= $estudiante['Direccion']['calle'] . ' ' . $estudiante['Direccion']['numeroint'] . ' Int: ' . $estudiante['Direccion']['numeroext'] . '<br>Col. ' . $estudiante['Direccion']['colonia'] . ' Del. ' . $estudiante['Direccion']['delegacion'] . '<br>C.P. ' . $estudiante['Direccion']['cp'] . ' ' . $estudiante['Direccion']['estado'] . ', ' . $estudiante['Direccion']['pais'] ?></h5>
            </div>
            <div class="card-body">
              <?= $this->Html->link('Editar correo electrónico', ['action' => 'edit', $estudiante->id], array('class' => 'btn btn-outline-primary')) ?>
              <?= $this->Html->link('Editar domicilio', ['controller' => 'Direccion', 'action' => 'edit', $estudiante['Direccion']['id']], array('class' => 'btn btn-outline-primary')) ?> <br>
              <small>Solo es posible editar el correo electrónico y el domucilio.</small>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card" style="margin-top: 25px; margin-left: 50px;">
            <div class="card-header">Cuenta</div>
            <div class="card-body">
              <p class="card-title">Nombre de usuario</p>
              <h5 class="card-text"><?= $estudiante['Users']['username'] ?></h5>
            </div>
            <div class="card-body">
              <?= $this->Html->link('Cambiar contraseña', ['controller' => 'Users', 'action' => 'edit', $estudiante['Users']['id']], array('class' => 'btn btn-outline-primary')) ?>
            </div>
        </div>
    </div>
</div>