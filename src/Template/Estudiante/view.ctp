<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['controller' => 'users', 'action' => 'index'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Gestionar alumnos', ['controller' => 'Users', 'action' => 'view'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'Users','action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div>
<div class="container">
    <table class="table table-sm">
        <thead class="thead-dark">
          <tr>
            <th class="text-center">Nombre del alumno</th>
            <th class="text-center">Nombre de usuario</th>
            <th class="text-center">Materias</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($estudiante as $estudiante): ?>
            <tr>
                <td class="text-center"><?= $estudiante->nombre . ' ' . $estudiante->apaterno . ' ' . $estudiante->amaterno ?></td>
                <td class="text-center"><?= $estudiante['Users']['username'] ?></td>
                <td class="text-center">
                    <?= $this->Html->link('Ver materias inscritas', ['action' => 'gestionar', $estudiante->id], array('class' => 'btn btn-sm btn-primary')) ?>
                </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primero')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, de un total de {{current}} estudiantes inscritos en el sistema')]) ?></p>
    </div>
</div>