<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?= $this->Html->link('Inicio', ['controller' => 'Users', 'action' => 'inicio'], array('class' => 'navbar-brand')) ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <?= $this->Html->link('Consultar horarios', ['controller' => 'Grupo', 'action' => 'index'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item">
              <?= $this->Html->link('Inscribir materias', ['controller' => 'Asignatura', 'action' => 'semestre'], array('class' => 'nav-link')) ?>
            </li>
            <li class="nav-item active">
              <?= $this->Html->link('Comprobante de inscripción', ['controller' => 'Estudiante', 'action' => 'comprobante'], array('class' => 'nav-link')) ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <?= $this->Html->link('Mi información', ['controller' => 'estudiante'], array('class' => 'btn btn-outline-info')) ?> &nbsp;
            <?= $this->Html->link('Cerrar sesión', ['controller' => 'Users','action' => 'logout'], array('class' => 'btn btn-outline-danger')) ?>
          </form>
        </div>
    </nav>
</div><?php $rows = $estudiante->count(); ?>
<div class="container">
    <h4 class="text-center" style="margin-top: 25px;">Universidad Nacional Autónoma de México</h4>
    <h4 class="text-center">Facultad de Derecho</h4>
    <h5 class="text-center">Carrera: <?= $datos['Carrera']['clave'] ?> Licenciatura en <?= $datos['Carrera']['nombre'] ?></h5>
    <h5 class="text-center">Administración escolar</h5>
    <h6 class="text-center">Comprobante de inscripción</h6>
    <h5><?= $datos->nombre . ' ' . $datos->apaterno . ' ' . $datos->amaterno ?></h5>
    <h5><?= $datos['Semestre']['semestre'] ?> semestre</h5>
    <table class="table table-sm">
        <thead class="thead-dark">
          <tr>
            <th class="text-center">Grupo</th>
            <th class="text-center">Clave de la asignatura</th>
            <th class="text-center">Nombre de la asignatura</th>
            <th class="text-center">Nombre del profesor</th>
            <th class="text-center">Día y hora</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($estudiante as $estudiante): ?>
            <tr>
                <td class="text-center"><?= $estudiante['Grupo']['clave'] ?></td>
                <td class="text-center"><?= $estudiante['Asignatura']['clave'] ?></td>
                <td class="text-center"><?= $estudiante['Asignatura']['nombre'] ?></td>
                <td class="text-center"><?= $estudiante['Profesor']['nombre'] . ' ' . $estudiante['Profesor']['apaterno'] . ' ' . $estudiante['Profesor']['amaterno'] ?></td>
                <td class="text-center"><?= $estudiante['Horario']['dia'] . ' ' . $estudiante['Horario']['hora'] ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
    </table>
    <?php
    if ($rows > 0) {
        echo "<button class='float-right btn btn-block btn-primary' onclick='imprimir()'>Imprimir comprobante</button>";
    } else {
        echo 'Aún no has inscrito ninguna materia';
    } ?>
</div>
<script>
function imprimir() {
  window.print();
}
</script>