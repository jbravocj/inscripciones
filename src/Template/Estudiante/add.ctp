<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Datos personales</title>
    <div>
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="">
              <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_fac.png" width="30" height="30" alt="">
              <img src="https://www.derecho.unam.mx/oferta-educativa/licenciatura/distancia/assets/imgs/logo_unam.png" width="30" height="30" alt="">
              Sistema de inscripciones de la Facultad de Derecho de la UNAM
            </a>
        </nav>
    </div>
    <div class="container">
        <?= $this->Form->create($estudiante) ?>
        <fieldset>
            <legend>Ingresa tus datos personales</legend>
            <?php
                echo $this->Form->control('nombre', ['label' => ['text' => 'Nombre(s)']]);
                echo $this->Form->control('apaterno', ['label' => ['text' => 'Apellido paterno']]);
                echo $this->Form->control('amaterno', ['label' => ['text' => 'Apellido materno']]);
                echo $this->Form->control('correo', ['label' => ['text' => 'Correo electrónico'], 'type' => 'email']);
            ?>
        </fieldset>
        <?= $this->Form->button('Guardar', array('class' => 'right btn btn-lg btn-success')) ?>
        <?= $this->Form->end() ?>
    </div>