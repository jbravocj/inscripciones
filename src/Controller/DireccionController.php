<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DireccionController extends AppController {
    public function index() {
        $direccion = $this->paginate($this->Direccion);

        $this->set(compact('direccion'));
    }

    public function view($id = null) {
        $direccion = $this->Direccion->get($id, [
            'contain' => []
        ]);

        $this->set('direccion', $direccion);
    }

    public function add() {
        $user = $this->getRequest()->getSession();
        $user->read('Auth.User.id');
        $estudiante = TableRegistry::get('Estudiante')->find()->select(['Estudiante.usuario', 'Estudiante.id'])
            ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();
        $direccion = $this->Direccion->newEntity();
        if ($this->request->is('post')) {
            $direccion = $this->Direccion->patchEntity($direccion, $this->request->getData());
            $direccion->estudiante = $estudiante->id;
            if ($this->Direccion->save($direccion)) {
                $this->Flash->success('La dirección de tu domicilio ha sido guardada con éxito.');
                return $this->redirect(['controller' => 'Users', 'action' => 'inicio']);
            }
            $this->Flash->error('Hubo un problema al intentar guardar tu dirección.');
        }
        $this->set(compact('direccion'));
    }

    public function edit($id = null){
        $direccion = $this->Direccion->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $direccion = $this->Direccion->patchEntity($direccion, $this->request->getData());
            if ($this->Direccion->save($direccion)) {
                $this->Flash->success('Se han guardado correctamente tus cambios.');

                return $this->redirect(['controller' => 'Estudiante', 'action' => 'index']);
            }
            $this->Flash->error(__('The direccion could not be saved. Please, try again.'));
        }
        $this->set(compact('direccion'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $direccion = $this->Direccion->get($id);
        if ($this->Direccion->delete($direccion)) {
            $this->Flash->success(__('The direccion has been deleted.'));
        } else {
            $this->Flash->error(__('The direccion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) {
            $allowedActions = ['add', 'edit'];
            if (in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }
        if ($user['tipo'] != 1) { 
            return true;
        }
    }
}