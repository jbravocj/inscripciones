<?php
namespace App\Controller;
use App\Controller\AppController;

class GrupoController extends AppController {
    public function index() {
        $grupo= $this->Grupo->find()->select(['Semestre.id', 'Semestre.semestre'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'Grupo.semestre = Semestre.id'])
                ->distinct(['Semestre.id']);
        
        $this->paginate($grupo);
        $this->set(compact('grupo'));
    }
    
    public function materia($idSemestre){ 
        $grupo= $this->Grupo->find()->select(['Asignatura.id', 'Asignatura.clave', 'Asignatura.nombre'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'asignatura', 'alias' => 'Asignatura',
                'type' => 'INNER', 'conditions' => 'Asignatura.id = ag.asignatura'])
            ->where(['semestre' => $idSemestre]);
        
        $this->paginate($grupo);
        $this->set('grupo', $grupo);
    }

    public function view($idMateria) {
        $grupo= $this->Grupo->find()->select(['clave', 'Asignatura.nombre', 'salon', 'cupo', 'Horario.dia', 'Horario.hora', 'Profesor.nombre', 'Profesor.apaterno', 'Profesor.amaterno'])
            ->join(['table' => 'grupoxhorario', 'alias' => 'gh',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gh.grupo'])
            ->join(['table' => 'horario', 'alias' => 'Horario',
                'type' => 'INNER', 'conditions' => 'Horario.id = gh.horario'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'asignatura', 'alias' => 'Asignatura',
                'type' => 'INNER', 'conditions' => 'Asignatura.id = ag.asignatura'])
            ->join(['table' => 'grupoxprofesor', 'alias' => 'gp',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gp.grupo'])
            ->join(['table' => 'Profesor', 'alias' => 'Profesor',
                'type' => 'INNER', 'conditions' => 'Profesor.id = gp.profesor'])
                ->where(['Asignatura.id' => $idMateria]);
        
        $this->paginate($grupo);
        $this->set('grupo', $grupo);
    }


    public function add(){
        $grupo = $this->Grupo->newEntity();
        if ($this->request->is('post')) {
            $grupo = $this->Grupo->patchEntity($grupo, $this->request->getData());
            if ($this->Grupo->save($grupo)) {
                $this->Flash->success(__('The grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The grupo could not be saved. Please, try again.'));
        }
        $this->set(compact('grupo'));
    }

    public function edit($id = null) {
        $grupo = $this->Grupo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupo = $this->Grupo->patchEntity($grupo, $this->request->getData());
            if ($this->Grupo->save($grupo)) {
                $this->Flash->success(__('The grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The grupo could not be saved. Please, try again.'));
        }
        $this->set(compact('grupo'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $grupo = $this->Grupo->get($id);
        if ($this->Grupo->delete($grupo)) {
            $this->Flash->success(__('The grupo has been deleted.'));
        } else {
            $this->Flash->error(__('The grupo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) {
            $allowedActions = ['index', 'materia', 'view'];
            if (in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }
        if ($user['tipo'] != 1) { 
            return true;
        }
    }
}