<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class EstudianteController extends AppController {
    public function index() {
        $user = $this->getRequest()->getSession();
        $estudiante = $this->Estudiante->find()->select(['id', 'nombre', 'apaterno', 'amaterno', 'correo', 'Users.id', 'Users.username', 'Direccion.id', 'Direccion.calle', 'Direccion.numeroint', 'Direccion.numeroext', 'Direccion.colonia', 'Direccion.delegacion', 'Direccion.cp', 'Direccion.estado', 'Direccion.pais'])
            ->join(['table' => 'users', 'alias' => 'Users',
                'type' => 'INNER', 'conditions' => 'Estudiante.usuario = Users.id'])
            ->join(['table' => 'direccion', 'alias' => 'Direccion',
                'type' => 'INNER', 'conditions' => 'Direccion.estudiante = Estudiante.id'])
                ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();
        
        $this->set('estudiante', $estudiante);
    }

    public function view($idSemestre = null) {
        $estudiante = $this->Estudiante->find()->select(['Users.username', 'id', 'nombre', 'apaterno', 'amaterno', 'semestre'])
            ->join(['table' => 'users', 'alias' => 'Users',
                'type' => 'INNER', 'conditions' => 'Estudiante.usuario = Users.id'])
                ->where(['Estudiante.semestre' => $idSemestre]);
        $this->paginate($this->Estudiante);
        $this->set('estudiante', $estudiante);
    }
    
    public function comprobante(){
        $user = $this->getRequest()->getSession();
        $datos = $this->Estudiante->find()->select(['Estudiante.usuario', 'Carrera.clave', 'Carrera.nombre', 'nombre', 'apaterno', 'amaterno', 'Semestre.semestre'])
            ->join(['table' => 'carrera', 'alias' => 'Carrera',
                'type' => 'INNER', 'conditions' => ' Estudiante.carrera = Carrera.id'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'Estudiante.semestre = Semestre.id'])
                ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();
        
        $estudiante = $this->Estudiante->find()->select(['Grupo.clave', 'Asignatura.clave', 'Asignatura.nombre', 'Profesor.nombre', 'Profesor.apaterno', 'Profesor.amaterno', 'Horario.dia', 'Horario.hora'])
            ->join(['table' => 'carrera', 'alias' => 'Carrera',
                'type' => 'INNER', 'conditions' => 'Estudiante.carrera = Carrera.id'])
            ->join(['table' => 'estudiantexasignatura', 'alias' => 'ea',
                'type' => 'INNER', 'conditions' => 'Estudiante.id = ea.estudiante'])
            ->join(['table' => 'asignatura', 'alias' => 'Asignatura',
                'type' => 'INNER', 'conditions' => 'Asignatura.id = ea.asignatura'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'ea.semestre = Semestre.id'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'ag.asignatura = Asignatura.id'])
            ->join(['table' => 'grupo', 'alias' => 'Grupo',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'grupoxhorario', 'alias' => 'gh',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gh.grupo'])
            ->join(['table' => 'horario', 'alias' => 'Horario',
                'type' => 'INNER', 'conditions' => 'Horario.id = gh.horario'])
            ->join(['table' => 'grupoxprofesor', 'alias' => 'gp',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gp.grupo'])
            ->join(['table' => 'Profesor', 'alias' => 'Profesor',
                'type' => 'INNER', 'conditions' => 'Profesor.id = gp.profesor'])
                ->where(['Estudiante.usuario' => $user->read('Auth.User.id')]);
        
        $this->set(compact('estudiante','datos', $datos));
    }

    public function add($userId) {
        $estudiante = $this->Estudiante->newEntity();
        if ($this->request->is('post')) {
            $estudiante = $this->Estudiante->patchEntity($estudiante, $this->request->getData());
            $estudiante->usuario = $userId;
            $estudiante->carrera = 1;
            $estudiante->semestre = 1;
            if ($this->Estudiante->save($estudiante)) {
                $this->Flash->success(__('Tus datos personales se han guardado con éxito.'));
                return $this->redirect(['controller' => 'Direccion', 'action' => 'add']);
            }
            $this->Flash->error('Hubo un error al guardar tus datos personales.');
        }
        $this->set(compact('estudiante'));
    }
    
    public function gestionar($idAlumno){
        $asignaturas = TableRegistry::get('estudiantexasignatura')->find()->select(['id', 'estudiante', 'Asignatura.nombre', 'semestre', 'estatus', 'Estudiante.nombre', 'Estudiante.apaterno', 'Estudiante.amaterno'])
            ->join(['table' => 'asignatura', 'alias' => 'Asignatura',
                'type' => 'INNER', 'conditions' => 'Asignatura.id = estudiantexasignatura.asignatura'])
            ->join(['table' => 'estudiante', 'alias' => 'Estudiante',
                'type' => 'INNER', 'conditions' => 'Estudiante.id = estudiantexasignatura.estudiante'])
                ->where(['estudiante' => $idAlumno]);
        $this->set(compact('asignaturas'));
        $this->paginate($asignaturas);
    }
    
    public function aprobar($idRegistro){
        $tabla = TableRegistry::get('estudiantexasignatura');
        $actualizar = $tabla->get($idRegistro);
        $actualizar->estatus = TRUE;
        if ($tabla->save($actualizar)) {
            $this->Flash->success('El alumno tiene estatus de aprobado en en la materia ahora.');
        } else {
            $this->Flash->success('Lo sentimos, hubo un error al realizar el cambio.');
        }
        
        return $this->redirect(['controller' => 'Users', 'action' => 'view']);
    }
    
    public function reprobar($idAlumno){
        $tabla = TableRegistry::get('estudiantexasignatura');
        $borrar = $tabla->get($idAlumno);
        if ($tabla->delete($borrar)) {
           $this->Flash->success('El alumno ha reprobado la materia ahora la puede volver a inscribir.');
        } else {
            $this->Flash->success('Lo sentimos, hubo un error al realizar el cambio.');
        }
        
        return $this->redirect(['controller' => 'Users', 'action' => 'view']);
    }

    public function edit($id = null) {
        $estudiante = $this->Estudiante->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estudiante = $this->Estudiante->patchEntity($estudiante, $this->request->getData());
            if ($this->Estudiante->save($estudiante)) {
                $this->Flash->success('Se ha guardado exitosamente tu nueva dirección de correo electrónico.');

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The estudiante could not be saved. Please, try again.'));
        }
        $this->set(compact('estudiante'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $estudiante = $this->Estudiante->get($id);
        if ($this->Estudiante->delete($estudiante)) {
            $this->Flash->success(__('The estudiante has been deleted.'));
        } else {
            $this->Flash->error(__('The estudiante could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) {
            $allowedActions = ['index', 'add', 'comprobante', 'edit'];
            if (in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }
        if ($user['tipo'] != 1) { 
            return true;
        }
    }
}