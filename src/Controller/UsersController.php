<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class UsersController extends AppController {
    public function inicio(){
        $user = $this->getRequest()->getSession();
        $this->set(compact('user'));
        $consulta = $this->Users->find()->select(['Estudiante.usuario'])
            ->join(['table' => 'estudiante', 'alias' => 'Estudiante',
                'type' => 'INNER', 'conditions' => 'Estudiante.usuario = Users.id'])
                ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();
        if ($consulta == false) {
            return $this->redirect(['controller'  => 'Estudiante', 'action' => 'add/' . $user->read('Auth.User.id')]);
        }
    }

    public function index(){
        $user = $this->getRequest()->getSession();
        $users = $this->paginate($this->Users);
        $this->set(compact('users', 'user'));
    }

    public function view(){
        $semestre = TableRegistry::get('Semestre')->find()->select(['Semestre.id', 'Semestre.semestre']);

        $this->set(compact('semestre'));
    }

    public function registro(){
        if($this->Auth->user()){
            $this->Flash->error(__('Ya te has registrado anteriormente')); return $this->redirect(['action' => 'inicio']);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->tipo = 1;
            if ($this->Users->save($user)) {
                $this->Flash->success('Tu registro se ha completado con éxito.');

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error('Hubo un error al guardar tu registro.');
        }
        $this->set(compact('user'));
    }

    public function edit($id = null){
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['controller' => 'Estudiante', 'action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function login(){
        if($this->Auth->user()){
            $this->Flash->error('Ya has iniciado sesión. No te puedes logear de nuevo.'); return $this->redirect(['action' => 'inicio']);
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                ($user['tipo'] === 2) ? $this->redirect(['action' => 'index']) : $this->redirect(['action' => 'inicio']);
            } else {
                $this->Flash->error('Hubo un error al iniciar sesión. Es posible que tu nombre de usuario y/o contraseña sean incorrectos.');
                $this->Flash->error('Solo cuentas con tres intentos para ingresar o se bloqueará tu cuenta por dos minutos');
            }
        }
    }
    
    public function logout(){ 
        $this->Flash->success('Has cerrado tu sesión.');
        return $this->redirect($this->Auth->logout());
    }
    
    public function creditos(){    }

    public function isAuthorized($user){
        if ($user['tipo'] == 1) {
            $allowedActions = ['inicio', 'logout', 'edit'];
            if (in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }
        if ($user['tipo'] != 1) { 
            return true; 
        }
    }
}