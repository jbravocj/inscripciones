<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class AsignaturaController extends AppController {
    public function index($idSemestre) {
        $user = $this->getRequest()->getSession();
        
        $idEstudiante = TableRegistry::get('estudiante')->find()->select(['id'])
            ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();
        
        $consulta = $this->Asignatura->find('list')->select(['ea.asignatura'])
            ->join(['table' => 'estudiantexasignatura', 'alias' => 'ea',
                'type' => 'INNER', 'conditions' => 'ea.asignatura = Asignatura.id'])
                ->where(['ea.estudiante' => $idEstudiante->id])
                ->enableHydration(false)->toArray();

        $asignatura = $this->Asignatura->find()->select(['id', 'clave', 'nombre'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'ag.asignatura = Asignatura.id'])
            ->join(['table' => 'grupo', 'alias' => 'Grupo',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'Grupo.semestre = Semestre.id'])
                ->where(['Grupo.semestre' => $idSemestre])->andWhere(['Asignatura.id NOT IN' => $consulta]);

        $this->paginate($asignatura);
        $this->set(compact('asignatura'));
    }
    
    public function semestre(){
        $user = $this->getRequest()->getSession();
        $semestre = TableRegistry::get('Estudiante')->find()->select(['Estudiante.semestre', 'Estudiante.usuario'])
            ->where(['Estudiante.usuario' => $user->read('Auth.User.id')])->first();

        $grupo= $this->Asignatura->find()->select(['Semestre.id', 'Semestre.semestre'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'ag.asignatura = Asignatura.id'])
            ->join(['table' => 'grupo', 'alias' => 'Grupo',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'Grupo.semestre = Semestre.id'])
                ->where(['Semestre.id <=' => $semestre->semestre])
                ->distinct(['Semestre.id'])
                ->order(['Semestre.id']);
        
        $this->paginate($grupo);
        $this->set(compact('grupo'));
    }

    public function view($idMateria){
        $user = $this->getRequest()->getSession();
        $asignatura = $this->Asignatura->find()->select(['id', 'nombre', 'Grupo.id', 'Grupo.clave', 'Grupo.salon', 'Grupo.cupo', 'Horario.dia', 'Horario.hora', 'Profesor.nombre', 'Profesor.apaterno', 'Profesor.amaterno', 'Semestre.id'])
            ->join(['table' => 'asignaturaxgrupo', 'alias' => 'ag',
                'type' => 'INNER', 'conditions' => 'ag.asignatura = Asignatura.id'])
            ->join(['table' => 'grupo', 'alias' => 'Grupo',
                'type' => 'INNER', 'conditions' => 'Grupo.id = ag.grupo'])
            ->join(['table' => 'grupoxhorario', 'alias' => 'gh',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gh.grupo'])
            ->join(['table' => 'horario', 'alias' => 'Horario',
                'type' => 'INNER', 'conditions' => 'Horario.id = gh.horario'])
            ->join(['table' => 'grupoxprofesor', 'alias' => 'gp',
                'type' => 'INNER', 'conditions' => 'Grupo.id = gp.grupo'])
            ->join(['table' => 'Profesor', 'alias' => 'Profesor',
                'type' => 'INNER', 'conditions' => 'Profesor.id = gp.profesor'])
            ->join(['table' => 'semestre', 'alias' => 'Semestre',
                'type' => 'INNER', 'conditions' => 'Grupo.semestre = Semestre.id'])
                ->where(['Asignatura.id' => $idMateria]);

        $this->paginate($asignatura);
        $this->set('asignatura', $asignatura);
        $this->set(compact('user'));
    }
    
    public function inscribir(){
        $usuario = $this->request->query['usuario'];
        $estudiante = TableRegistry::get('Estudiante')->find()->select(['Estudiante.id', 'Estudiante.usuario'])->where(['Estudiante.usuario' => $usuario])->first();
        $asignatura = $this->request->query['asignatura'];
        $grupo = $this->request->query['grupo'];
        $semestre = $this->request->query['semestre'];
        $registro = TableRegistry::get('estudiantexasignatura');
        $query = $registro->query();
        $query->
            insert(['estudiante', 'asignatura', 'semestre'])
            ->values([
                'estudiante' => $estudiante->id,
                'asignatura' => $asignatura,
                'semestre' => $semestre]);
        $cupo = TableRegistry::get('grupo');
        $actualizar = $cupo->get($grupo);
        $actualizar->cupo = $actualizar->cupo - 1;
        $cupo->save($actualizar);
        if($query->execute()){
            $this->Flash->success('Se ha completado tu inscripción correctamente');
        } else {
            $this->Flash->success('Lo sentimos, hubo un error al registrar tu inscripción');
        }
        
        return $this->redirect(['action' => 'semestre']);
    }

    public function add() {
        $asignatura = $this->Asignatura->newEntity();
        if ($this->request->is('post')) {
            $asignatura = $this->Asignatura->patchEntity($asignatura, $this->request->getData());
            if ($this->Asignatura->save($asignatura)) {
                $this->Flash->success(__('The asignatura has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The asignatura could not be saved. Please, try again.'));
        }
        $this->set(compact('asignatura'));
    }

    public function edit($id = null) {
        $asignatura = $this->Asignatura->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asignatura = $this->Asignatura->patchEntity($asignatura, $this->request->getData());
            if ($this->Asignatura->save($asignatura)) {
                $this->Flash->success(__('The asignatura has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The asignatura could not be saved. Please, try again.'));
        }
        $this->set(compact('asignatura'));
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $asignatura = $this->Asignatura->get($id);
        if ($this->Asignatura->delete($asignatura)) {
            $this->Flash->success(__('The asignatura has been deleted.'));
        } else {
            $this->Flash->error(__('The asignatura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) {
            $allowedActions = ['index', 'semestre', 'view', 'add', 'inscribir'];
            if (in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }
        if ($user['tipo'] != 1) { 
            return true;
        }
    }
}