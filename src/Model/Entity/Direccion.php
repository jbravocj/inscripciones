<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Direccion Entity
 *
 * @property int $id
 * @property int $estudiante
 * @property string $calle
 * @property string $numeroint
 * @property string|null $numeroext
 * @property string $colonia
 * @property string $delegacion
 * @property string $cp
 * @property string $estado
 * @property string $pais
 */
class Direccion extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'estudiante' => true,
        'calle' => true,
        'numeroint' => true,
        'numeroext' => true,
        'colonia' => true,
        'delegacion' => true,
        'cp' => true,
        'estado' => true,
        'pais' => true
    ];
}
