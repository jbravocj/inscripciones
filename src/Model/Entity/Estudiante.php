<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Estudiante Entity
 *
 * @property int $id
 * @property int $usuario
 * @property string $nombre
 * @property string $apaterno
 * @property string $amaterno
 * @property string $correo
 * @property int $carrera
 */
class Estudiante extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'usuario' => true,
        'nombre' => true,
        'apaterno' => true,
        'amaterno' => true,
        'correo' => true,
        'carrera' => true
    ];
}
