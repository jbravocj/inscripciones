<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Asignatura Entity
 *
 * @property int $id
 * @property string $clave
 * @property string $nombre
 */
class Asignatura extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'clave' => true,
        'nombre' => true
    ];
}
