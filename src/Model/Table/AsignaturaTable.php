<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Asignatura Model
 *
 * @method \App\Model\Entity\Asignatura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Asignatura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Asignatura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Asignatura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asignatura saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asignatura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Asignatura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Asignatura findOrCreate($search, callable $callback = null, $options = [])
 */
class AsignaturaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('asignatura');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('clave')
            ->maxLength('clave', 8)
            ->requirePresence('clave', 'create')
            ->allowEmptyString('clave', false);

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 30)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false);

        return $validator;
    }
}
