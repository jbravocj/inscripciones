<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Direccion Model
 *
 * @method \App\Model\Entity\Direccion get($primaryKey, $options = [])
 * @method \App\Model\Entity\Direccion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Direccion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Direccion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Direccion saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Direccion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Direccion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Direccion findOrCreate($search, callable $callback = null, $options = [])
 */
class DireccionTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('direccion');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('estudiante')
            ->requirePresence('estudiante', 'create')
            ->allowEmptyString('estudiante', false);

        $validator
            ->scalar('calle')
            ->maxLength('calle', 50)
            ->requirePresence('calle', 'create')
            ->allowEmptyString('calle', false);

        $validator
            ->scalar('numeroint')
            ->maxLength('numeroint', 10)
            ->requirePresence('numeroint', 'create')
            ->allowEmptyString('numeroint', false);

        $validator
            ->scalar('numeroext')
            ->maxLength('numeroext', 10)
            ->allowEmptyString('numeroext');

        $validator
            ->scalar('colonia')
            ->maxLength('colonia', 50)
            ->requirePresence('colonia', 'create')
            ->allowEmptyString('colonia', false);

        $validator
            ->scalar('delegacion')
            ->maxLength('delegacion', 50)
            ->requirePresence('delegacion', 'create')
            ->allowEmptyString('delegacion', false);

        $validator
            ->scalar('cp')
            ->maxLength('cp', 5)
            ->requirePresence('cp', 'create')
            ->allowEmptyString('cp', false);

        $validator
            ->scalar('estado')
            ->maxLength('estado', 22)
            ->requirePresence('estado', 'create')
            ->allowEmptyString('estado', false);

        $validator
            ->scalar('pais')
            ->maxLength('pais', 22)
            ->requirePresence('pais', 'create')
            ->allowEmptyString('pais', false);

        return $validator;
    }
}
