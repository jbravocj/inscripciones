<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Estudiante Model
 *
 * @method \App\Model\Entity\Estudiante get($primaryKey, $options = [])
 * @method \App\Model\Entity\Estudiante newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Estudiante[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Estudiante|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Estudiante saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Estudiante patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Estudiante[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Estudiante findOrCreate($search, callable $callback = null, $options = [])
 */
class EstudianteTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('estudiante');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('usuario')
            ->requirePresence('usuario', 'create')
            ->allowEmptyString('usuario', false);

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 30)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false);

        $validator
            ->scalar('apaterno')
            ->maxLength('apaterno', 30)
            ->requirePresence('apaterno', 'create')
            ->allowEmptyString('apaterno', false);

        $validator
            ->scalar('amaterno')
            ->maxLength('amaterno', 30)
            ->requirePresence('amaterno', 'create')
            ->allowEmptyString('amaterno', false);

        $validator
            ->scalar('correo')
            ->maxLength('correo', 50)
            ->requirePresence('correo', 'create')
            ->allowEmptyString('correo', false);

        $validator
            ->integer('carrera')
            ->requirePresence('carrera', 'create')
            ->allowEmptyString('carrera', false);

        return $validator;
    }
}
